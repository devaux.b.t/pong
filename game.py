from game_object import GameObject
from objects.paddle import Paddle
from pygame.surface import Surface
from objects.ball import Ball
from objects.score import Score


# Une classe qui va gérer la logique du jeu


class Game(GameObject):
    # On crée nos objets
    ball = Ball()
    #score = Score(0,0)
    player_paddle = Paddle(ball, True)
    enemy_paddle = Paddle(ball, False)

    # Indique si la boucle de jeu doit tourner ou non
    should_run = True

    # Lance les init de chaque objet au démarrage du jeu
    def init(self, screen: Surface):
        self.ball.init(screen)
        self.player_paddle.init(screen)
        self.enemy_paddle.init(screen)
        #self.score.init(screen)

    # Lance les update de chaque objet à chaque frame et gère la
    # relation entre les objets
    def update(self):
        self.ball.update()
        self.player_paddle.update()
        self.enemy_paddle.update()
        #self.score.update()

        # gestion de la collision entre ball et les paddles
        if (self.ball.as_rect().colliderect(self.player_paddle.rect)):
                self.ball.flagX= False
        if (self.ball.as_rect().colliderect(self.enemy_paddle.rect)):
                self.ball.flagX= True
        if (self.ball.pos.x>800):
            print("J2 Loose")
            #self.score.score1 +=1
            #if self.score.score1 == 3:
            #    should_run = False
            #else:
            #    self.init()
            exit()
        if (self.ball.pos.x<0):
            print("J1 Loose")
            #self.score.score2 +=1
            #if self.score.score2 == 3:
            #    should_run = False
            #else:
            #    self.init()
            exit()
            #test
            