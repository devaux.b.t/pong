from pygame import display
import pygame
from game_object import GameObject


# Définition de la classe Paddle
class Score(GameObject):

    # Constructeur de la classe
    def __init__(self, score1, score2):
        # Position de la raquette
        self.font=pygame.font.Font(None, 50)
        self.score1= score1
        self.score2= score2
        self.affichage = font.render(self.score1+" | "+self.score2,1,(255,255,255))
    # Méthode d'initialisation de l'objet, à exécuter une fois au début
    def init(self, screen):
        self.screen = screen
        self.screen.blit(self.affichage, (360, 25))
        pygame.display.flip()

    # Méthode de mise à jour de l'objet, à exécuter à chaque image
    def update(self):
        # Il existe une fonction par type de joueur, manuel ou IA
        pygame.display.flip()

    